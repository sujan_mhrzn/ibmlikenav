//
$(function(){
    // navigation
    function sideNavbar(){
        //toggle
        $(".navbar-trigger").on("click",function(){
            $(".navbar-menu.mobile").addClass("show-menu");
            $(".c-overlay").addClass("active navbar-overlay");
        });
        $(".navbar-menu__close").on("click",function(){
            $(".navbar-menu.mobile").removeClass("show-menu");
            $(".c-overlay").removeClass("active navbar-overlay");
        });
        $(document).on("click",".navbar-overlay",function(){
            $(".navbar-menu.mobile").removeClass("show-menu");
            $(".c-overlay").removeClass("active navbar-overlay");
        })
    }

    // megamenu
    function megamenu(){
        $(document).on("mouseover",".menu-list .menu-item",function(){
            var _target=$(this).attr("data-megamenu");
            $(this).closest(".menu-list").find(".menu-item").removeClass("active");
            $(this).closest(".menu-list").find(".megamenu-item").removeClass("active");
            $(this).addClass("active");
            $(".megamenu-item#"+_target).addClass("active");
        });
    }
    // accordion
    function accordion(){
        if($(".c-accordion").length){
            $(".c-accordion__title").on("click",function(e){
                if($(this).next(".c-accordion__body").is(":visible")){
                    $(this).removeClass("is-active").closest(".accordion").removeClass("active");
                    $(this).next(".c-accordion__body").slideUp();
                }else{
                    $(this).addClass("is-active").closest(".accordion").addClass("active");
                    $(this).next(".c-accordion__body").slideDown();
                }
            });
        }
    }

    function init(){
        sideNavbar();
        megamenu();
        accordion();
    }
    init();
});